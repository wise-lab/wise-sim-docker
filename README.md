# wise-sim-docker

A Docker wrapper around [wise-sim-scenarios](https://git.uwaterloo.ca/wise-lab/wise-sim-scenarios) allowing for running WISE ADS and WISE Sim simulation scenarios.

## License

All content in this repository is provided under the MIT license (see LICENSE).

All content in the binary ("object") form, including

  * WISE Sim,
  * WISE ADS,
  * `wise-sim-docker-image.tar.xz` Docker image, which includes WISE ADS and Sim,

are provided under the MIT license.

# Installation

## Install prerequisites

1. Install Docker by following [official instructions](https://docs.docker.com/engine/install/ubuntu/).

**NOTE1:** After adding your user to the group `docker`, log out and in for the change to take effect.
Verify you can execute `$ docker run hello-world` without `sudo`.

2. Install NVidia Docker by following [official instructions](https://github.com/NVIDIA/nvidia-docker#quickstart).

**NOTE2:** Verify you can execute `$ docker run --gpus all nvidia/cuda:11.3.0-base nvidia-smi` and see the same output
as when executing `$ nvidia-smi` on your host system.

## Install `wise-sim-docker`

1. Clone the respository including the submodule.

```
$ git clone --recursive https://git.uwaterloo.ca/wise-lab/wise-sim-docker
```

**NOTE1:** If you cloned without the `--recursive`, you have to execute submodule initialization as follows:

```
$ cd wise-sim-docker
$ git submodule update --init --recursive
```

2. You can either download a pre-built image or build the image locally.

```
$ cd wise-sim-docker
```

2a. To download a pre-built image (over 2.1 Gb), execute
```
$ bash download-docker-image.bash
```

2b. To build a docker image `wise-sim:focal-noetic`

```
$ bash docker-build.bash
```

# Usage

1. Start a container from the image `wise-sim:focal-noetic` to enter an interactive session.

```
$ bash docker-start.bash
```

This session allows for starting a simulation scenario.
Follow [WISE Sim Scenarios README](https://git.uwaterloo.ca/wise-lab/wise-sim-scenarios/#usage).

**NOTE1**: Make sure to start the scenario in headless mode using `-H` or `--headless` parameter.

**NOTE2**: Start RViz and RQT outside of the container to view the simulation.
Use RViz configuration `wise-sim-scenarios/rviz/wise-ads.rviz`.
It is good to save it as the default RViz configuration `$HOME/.rviz/default.rviz`.

2. Open additional interactive sessions using

```
$ bash docker-enter.bash
```

**NOTE3**: the command finds the last container started from the image `wise-sim:focal-noetic`.

# Troubleshooting

## Simulator window does not show

Make sure to run in the headless mode. See [Usage, NOTE1](#usage).

## No RViz or RViz does not show any outputs

Start RViz on the host machine.
Make sure to open the `wise-sim-scenarios/rviz/wise-ads.rviz` configuration to see
WISE ADS and Sim data.
Save `wise-ads.rviz` as the default in `$HOME/.rviz/default.rviz` so that it is
automatically used when starting RViz.

## RViz started and properly configured by no car or the car is not driving

The simulator might have crashed.
To view the status of the simulator, enter the container using `docker-enter.bash`
and execute `screen -r` to reattach to the screen session in which the simulator is run.

To upgrade the simulator to the latest version,
remove the cached `*.tar.xz` files from `wise-sim-scenarios/downloads`
and rebuild the container, which will download the latest versions.
