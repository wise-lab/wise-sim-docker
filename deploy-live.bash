#!/bin/bash
set -e

USER="gitlab-runner"
TARGET_SERVER="$USER@wiselab.uwaterloo.ca"
TARGET_DIR="/data/wise-sim/"
ARCHIVE_NAME="wise-sim-docker-image.tar.xz"

echo "Deploying ${ARCHIVE_NAME} to ${TARGET_SERVER}:${TARGET_DIR}"
ssh -p 14322 ${TARGET_SERVER} "mv /home/gitlab-runner/${ARCHIVE_NAME} ${TARGET_DIR}"
