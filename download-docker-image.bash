#!/bin/bash

# Image should be saved using the following command
# $ docker save <image ID> wise-sim:focal-noetic | xz -9 -T8 > downloads/wise-sim-docker-image.tar.xz
# And uploaded to wiselab.uwaterloo.ca/wise-sim/wise-sim-docker-image.tar.xz

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DOWNLOADS_DIR="$SCRIPT_DIR/downloads"

IMAGE_NAME="wise-sim:focal-noetic"
IMAGE_FILE_NAME="wise-sim-docker-image.tar.xz"


if [[ -f "$DOWNLOADS_DIR/$IMAGE_FILE_NAME" ]]; then
    echo "Using already downloaded image $DOWNLOADS_DIR/$IMAGE_FILE_NAME..."
    echo "Remove the file and re-run the script to download a newer version."
    echo ""
    read -t 10 -n 1 -s -r -p "Press <CTRL>+c to cancel. Press any key to continue. Continuing in 10s..."
else
    echo "Downloading $IMAGE_NAME from the server... (2.1 Gb)"
    SERVER_ADDR="wiselab.uwaterloo.ca"
    if ping -c 1 -W 1 "$SERVER_ADDR" &> /dev/null; then
        cd "$DOWNLOADS_DIR"
        wget "$SERVER_ADDR/wise-sim/$IMAGE_FILE_NAME"
    else
        echo "ERROR: The server $SERVER_ADDR cannot be reached. Aborting..."
        exit 1
    fi
fi

echo ""
echo "Loading the image... Please wait..."

trap 'docker_load_error  $? $LINENO' ERR
docker_load_error() {
    echo "Error $1 on line $2: loading of docker image from file '$DOWNLOADS_DIR/$IMAGE_FILE_NAME' failed."
    exit 1
}
set -x
xz --decompress --stdout -T8 "$DOWNLOADS_DIR/$IMAGE_FILE_NAME" | docker load
