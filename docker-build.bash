#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

IMAGE_NAME="wise-sim:focal-noetic"

echo "Building docker image '${IMAGE_NAME}'"

# ensure previous wise-sim-docker-image.tar.xz not passed into the new image
rm "${SCRIPT_DIR}/downloads/*"
docker build -t ${IMAGE_NAME} ${SCRIPT_DIR}

if [ $? -ne 0 ]; then
    echo "Error: docker image '${IMAGE_NAME}' build failed"
    exit 1
fi
