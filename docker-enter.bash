#!/bin/bash

if [[ "$1" == "--help" ]]; then
    echo "Enter an interactive session in the last container started using docker-start.bash"
fi

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

USER_ID=$(id -u)
GROUP_ID=$(id -g)
CONTAINER_ID=$(docker ps --latest --filter ancestor=wise-sim:focal-noetic --filter status=running --quiet )

if [[ -n "$CONTAINER_ID" ]]; then
    docker exec -it \
           --user "$USER_ID:$GROUP_ID" \
           $CONTAINER_ID \
           /bin/bash --login
else
    echo "No running container from image 'wise-sim:focal-noetic'."
    echo "Run 'bash docker-start.bash' to start the container first."
    echo ""
    exit 1
fi
