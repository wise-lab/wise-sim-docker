#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

IMAGE_NAME="wise-sim:focal-noetic"

USER_ID=$(id -u)
GROUP_ID=$(id -g)
TIMEZONE=`cat /etc/timezone`

docker run -it \
       --env "USER=$USER" \
       --env "USER_ID=$USER_ID" \
       --env "GROUP=$USER" \
       --env "GROUP_ID=$GROUP_ID" \
       --env "TIMEZONE=$TIMEZONE" \
       --volume="$SCRIPT_DIR/wise-sim-scenarios:/home/$USER/wise-sim-scenarios" \
       --gpus all \
       --net=host \
       --env="DISPLAY=$DISPLAY" \
       --env="QT_X11_NO_MITSHM=1" \
       --volume="$HOME/.Xauthority:/home/$USER/.Xauthority:rw" \
       --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
       $IMAGE_NAME

docker rm ${IMAGE_NAME}
