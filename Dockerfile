FROM adamrehn/ue4-runtime:20.04-vulkan

# switch back to root from ue4
USER root

ENV TERM xterm
ENV CONTINUOUS_INTEGRATION true
ENV TZ=Etc/UTC
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8

RUN DEBIAN_FRONTEND=noninteractive apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get -qq -y install apt-utils locales tzdata \
 && ln -sfn /usr/share/zoneinfo/Etc/UTC /etc/localtime \
 && locale-gen en_US.UTF-8; \
     dpkg-reconfigure -f noninteractive locales; \
     dpkg-reconfigure -f noninteractive tzdata \
 && echo 'Etc/UTC' > /etc/timezone \
 && echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -y \
 && DEBIAN_FRONTEND=noninteractive apt-get install -qq --no-install-recommends \
                    software-properties-common \
                    ca-certificates \
                    apt-transport-https \
                    dirmngr \
                    gnupg2 \
                    lsb-release \
                    pkg-config \
                    sudo \
                    udev \
                    unzip \
                    wget \
                    curl \
                    iputils-ping \
                    gosu \
                    xz-utils \
                    psmisc \
 && DEBIAN_FRONTEND=noninteractive apt-get clean



# After apt-get install -qq sudo
RUN echo 'ALL ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

COPY wise-sim-scenarios/downloads /downloads
COPY wise-sim-scenarios/scripts /scripts

ENV ROS_DISTRO noetic

RUN bash /scripts/wise_ads_install.bash
RUN bash /scripts/install_wise_sim.bash

COPY entrypoint /scripts/entrypoint

ENTRYPOINT ["/scripts/entrypoint"]

CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
