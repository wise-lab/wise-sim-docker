#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
IMAGE_NAME="wise-sim:focal-noetic"

docker save ${IMAGE_NAME} | xz -9eT0 > "${SCRIPT_DIR}/downloads/wise-sim-docker-image.tar.xz"
docker rmi ${IMAGE_NAME}
